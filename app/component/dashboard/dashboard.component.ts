/**
 * Created by djavrell on 07/04/16.
 */

import  {Component, OnInit}  from 'angular2/core';
import  {Router}  from 'angular2/router';

import  {Hero}          from 'app/hero';
import  {HeroService}   from 'app/service/hero.service';

@Component({
    selector: 'my-dashboard',
    templateUrl: 'app/component/dashboard/dashboard.component.html',
    styleUrls: ['app/component/dashboard/dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    heroes: Hero[] = [];

    constructor(
        private _heroService: HeroService,
        private _router: Router
    ) {}

    ngOnInit() {
        this._heroService
            .getHeroes()
            .then(heros => this.heroes = heros.slice(1, 5))
    }

    goToDetail(hero: Hero) {
        let link = ['HeroDetail', {id: hero.id}];
        this._router.navigate(link);
    }
}
