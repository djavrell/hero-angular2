import {Component, OnInit}      from 'angular2/core';
import {Router}                 from 'angular2/router';

import {Hero}                   from 'app/hero';
import {HeroDetailComponent}    from 'app/component/hero-detail/hero-detail.component';
import {HeroService}            from 'app/service/hero.service';

@Component({
    selector: 'my-heroes',
    templateUrl: 'app/component/heroes/heroes.component.html',
    styleUrls: ['app/component/heroes/heroes.component.css'],
    directives: [HeroDetailComponent],
})
export class HeroesComponent implements OnInit {
    constructor(
        private _heroService: HeroService,
        private _router: Router
    ) {}

    heroes: Hero[];
    selectedHero: Hero;

    getHeroes() {
        this._heroService
            .getHeroes()
            .then(heroes => this.heroes = heroes);
    }

    onSelect(hero: Hero) {
        this.selectedHero = hero;
    }

    ngOnInit() {
        this.getHeroes();
    }

    goToDetail() {
        this._router.navigate(['HeroDetail', {id: this.selectedHero.id}]);
    }
}
